'''
Processes dataset and returns or saves feature vectors
'''

import os
import time
import numpy as np
import utils as u
from skimage.restoration import denoise_wavelet
from skimage.io import imread
from skimage.color import rgb2gray
from skimage import feature
from matplotlib import pyplot as plt
from multiprocessing import Process, Manager

datadir = '/home/diedre/bigdata/camera'
traindir = datadir + '/train'
testdir = datadir + '/test'

original_cams = []

cam_dirs = []
cam_names = []
cam_images = {}
manager = Manager()
mean_noises = manager.dict()
cam_features = manager.dict() #will numpy append these to a general train vector
features_res = manager.dict()
val_features = manager.dict()
test_features = manager.dict()

# Main output of this module: 
xtrain = [] # array of arrays of features from each x input with its result 
xval = [] # array of validation features with results
xtest = []
def defineVariables(testmode):
    global cam_dirs
    global cam_names
    global cam_images
    cam_dirs = []
    cam_names = []
    cam_images = {}
    if testmode is True:
        dire = testdir
    else:
        dire = traindir
    for root, dirs, _ in os.walk(dire, topdown=False):
        for name in dirs:
            path = os.path.join(root, name)
            name = os.path.basename(path)
            cam_dirs.append(path)
            cam_names.append(name)
            cam_images[name] = os.listdir(path)


def saveMeanNoises():
    for cam, noise in mean_noises.items():
        np.save("means/"+cam, noise)

def loadFeatures():
    return np.load("xtrain.npy"), np.load("xval.npy"), np.load("xtest.npy")


def loadMeans(show=False):
    for cam in original_cams:
        mean_noises[cam] = np.load("means/" + cam +".npy")
        print("loaded mean noise for " + cam)

def yellProgramState(calc_mean, multithread, skip_train, sleeptime):
    if calc_mean is True:
        print("CALCULATING MEAN NOISES!")
    if multithread is True:
        print("USING MULTIPLETHREADS!")
    if skip_train is True:
        print("ATTENTION, SKIPPING TRAINING DATA")
    #time.sleep(sleeptime)

def extract(calculate_mean_noise, parallel, skip_train, max_correlations, testmode):
    # Processing images
    ps = []
    extract_start = time.time()
    if calculate_mean_noise is False:
        loadMeans()
    for cam, images in cam_images.items(): #defined in define variables
        # Each of this loop is one folder
        # Insert multithreading here
        if parallel is True:
            p = Process(target=processFolder, args=(cam, images, False, False, mean_noises, val_features, cam_features, test_features, calculate_mean_noise, skip_train, max_correlations, testmode))
            ps.append(p)
            p.start()
        else:
            processFolder(cam, images, False, False, mean_noises, val_features, cam_features, test_features, calculate_mean_noise, skip_train, max_correlations, testmode)


    if parallel is True:
        for p in ps:
            p.join()

    if calculate_mean_noise is True:
        saveMeanNoises()  

    
    if testmode is True:
        print("Creating test data")
        for _, features in test_features.items():
            xtest.append(features)    
        np.save("xtest", xtest)
        np.savetxt("xtest.csv", xtest, delimiter=",", fmt='%s')
        
    else:
        print("Creating training data")
        for _, features in cam_features.items():
            xtrain.append(features)
        for _, features in val_features.items():
            xval.append(features)   
        np.savetxt("xtrain.csv", xtrain, delimiter=",", fmt='%s')
        np.save("xtrain", xtrain)
        np.savetxt("xval.csv", xval, delimiter=",", fmt='%s')  
        np.save("xval", xval)     

    
        
    print("Time to extract features: " + str(time.time() - extract_start))

#---------------------------- Process Folder --------------------------------#

def processFolder(cam, images, verbose, display, mean_noises, val_features, cam_features, test_features,
                  calculate_mean_noise, skip_train_data, max_correlations, testmode):
    '''
    Goes through a folder of images,
    Receives folder name and path
    '''
    #lbp = u.LocalBinaryPatterns(8, 1)
    noise_accumulator = np.zeros((512, 512, 3))
    number_of_images = len(images)
    processed = 0
    positivetest = 0
    negativetest = 0
    ratio = 0
    trainths = 80
    print("Now processing camera " + cam)
    for img in images:
        if skip_train_data is False or ratio > trainths:
            # Form path
            if testmode is True:
                imgpath = testdir + "/" + cam + "/" + img
                imgdata = plt.imread(imgpath)
            else:
                imgpath = traindir + "/" + cam + "/" + img
                imgdata = imread(imgpath)  
            
            
            # Read and crop image

            
            if cam == "Sony-NEX-7":
                imgdata = imgdata[0]
            imgcrop = u.cropCenter(imgdata, (512, 512))    

            # Get denoised image
            denoised_color = (denoise_wavelet(imgcrop, multichannel=True, convert2ycbcr=True)*255)
            
            # Get isolated noise
            noise = imgcrop - denoised_color

            #LBP of image
            #graynoise = rgb2gray(imgcrop)
            #LBP = feature.local_binary_pattern(graynoise,8 , 1)
            #(hist, _) = np.histogram(LBP.ravel(), bins=256, density=True)

            #LBP of 3channel img
            #graynoise = rgb2gray(noise)
            #LBP = feature.local_binary_pattern(graynoise,8 , 1)
            #(hist, _) = np.histogram(LBP, bins=256, density=True)
            
            #patches = (noise[0:255, 0:255, :],
            #           noise[0:255, 256:511, : ],
            #           noise[256:511, 0:255, :],
            #           noise[256:511, 256:511, :])

            #hists = []
            #for i in range(0,3):
            #    LBP = feature.local_binary_pattern(imgcrop[:, :, i], 8, 1)
            #    hists.append(np.histogram(LBP.ravel(), bins=256, range=(0,255))[0])

        if ratio > trainths and testmode is False:
            # Work on validation set after 80% of data in trainmode
            if calculate_mean_noise is True:
                print(cam + " finished mean noise calculation")
                break
            else:
                if max_correlations is True:
                    maxcc = -999
                    maxcam = ""
                    for mcam, mnoise in mean_noises.items():
                        cc = u.correlation_coefficient(noise, mnoise)
                        if cc > maxcc:
                            maxcc = cc
                            maxcam = mcam
                    if (maxcam == cam):
                        positivetest += 1
                    else:
                        negativetest += 1
                else:
                    #Validation features
                    ccs = []
                    for _, mnoise in mean_noises.items(): # correlations
                        ccs.append(u.correlation_coefficient(noise, mnoise))
                    
                    #for value in hist: #one hist
                        #ccs.append(value)    
                    
                    #for hist in hists: #channel hists
                    #    for value in hist:
                    #        ccs.append(value)

                    ccs.append(cam)
                    val_features[img] = ccs

        else:
            if calculate_mean_noise is True:
                #noise_accumulator += noise
                pass
            else:
                #Training or testing features
                ccs = []

                # Main features, correlation with mean noises
                for _, mnoise in mean_noises.items():
                    ccs.append(u.correlation_coefficient(noise, mnoise))
                
                # LBP
                #for value in hist:
                #    ccs.append(value)   
                
                #for hist in hists: #channel hists
                #        for value in hist:
                #            ccs.append(value)

                #Append expected result and put in main dict
                if testmode is True:
                    ccs.append(img)
                    test_features[img] = ccs
                else:
                    ccs.append(cam)
                    cam_features[img] = ccs
        processed += 1
        ratio = round(processed*100/number_of_images, 1)
        if ratio % 20 == 0:
            print("Batch " + cam + ": " + str(ratio) + " %...")
    if max_correlations is True:
        print("Max correlation classification accuracy for: " + cam + ": " + str(round(positivetest*100/(negativetest + positivetest), 2)) + " %")
        print()
    if calculate_mean_noise is True:
        mean_noises[cam] = noise_accumulator/number_of_images

#---------------------------- Main --------------------------------#

def init(calc_mean, multithread, skip_train, max_correlations, loadfeatures, test_only):
    global xtrain, xval, xtest, original_cams
    defineVariables(testmode=False)
    # Detect folders and images
    print("Detected phones: " + str(cam_names))
    for cams in cam_names:
        original_cams.append(cams) #deep copy
    
    #Yell variables just to make sure
    yellProgramState(calc_mean, multithread, skip_train, 2)

    # Training
    if loadfeatures is True:
        xtrain, xval, xtest = loadFeatures()
    else:
        if test_only is False:
            extract(calc_mean, multithread, skip_train, max_correlations, False)
        quit()
        defineVariables(testmode=True)
        print("Extracting features from test dataset")
        extract(calc_mean, multithread, skip_train, max_correlations, True)

    if test_only is True:
        xtrain, xval, xtest = loadFeatures() 

    xtrain = np.asarray(xtrain)
    xval = np.asarray(xval)
    xtest = np.asarray(xtest)
    print("dataset initialized", end='\n\n')
    print("training shape")
    print(xtrain.shape)
    print("validation shape")
    print(xval.shape)        
    print("test shape")
    print(xtest.shape)  
    print("dataset initialized")   
#TODO features:
# Correlation 0 to 9 (10 features)
#parallelExtract()
