import dataset
import numpy as np
import utils as u
from math import isnan 
import csv
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import SGDClassifier

from multiprocessing import Process

calc_mean = False
skip_train = False
max_correlations = True

multithread = True
loadfeatures = False
test_only = False  #train test only

dataset.init(calc_mean, multithread, skip_train, max_correlations, loadfeatures, test_only)



training = dataset.xtrain
nfeatures = training.shape[1] - 1
tfeatures = training.T[0:nfeatures].T[:].astype(float) # get everything before last column
tclass = training[:,-1].astype(str) # get only last column

print("training features shape")
print(tfeatures.shape)
print("training results shape")
print(tclass.shape)



validation = dataset.xval
vfeatures = validation.T[0:nfeatures].T[:].astype(float) # get everything before last column
vclass = validation[:,-1].astype(str) # get only last column
print("validation features shape")
print(vfeatures.shape)
print("validation results shape")
print(vclass.shape)

test = dataset.xtest
testfeatures = test.T[0:nfeatures].T[:].astype(float)
imgs = test[:,-1].astype(str)
mask = np.isnan(testfeatures)
testfeatures[mask] = 0.001
print("test features shape")
print(testfeatures.shape)

def testModel(modeltype, name, max_iter=5000, regparam=0.0001, lrate=0.0001, layers=400, loss="log"):
    if modeltype == "mlp":
        model = MLPClassifier(solver='sgd', activation='logistic', max_iter=max_iter, hidden_layer_sizes=(layers,), alpha=regparam, learning_rate_init=lrate)
    elif modeltype == "logreg":
        model = SGDClassifier(max_iter=max_iter, loss=loss, penalty='l2', learning_rate='constant', eta0=lrate, alpha=regparam)
    elif modeltype == "logistic":
        model = LogisticRegression()
    elif modeltype == "adam":
        model = MLPClassifier(hidden_layer_sizes=(layers,), activation='logistic', alpha=regparam, learning_rate_init=lrate)
    else:
        raise ValueError("model is wrong.")
    model.fit(tfeatures, tclass)
    print(name)
    print("training score: " + str(model.score(tfeatures, tclass)))
    print("validation score LR: " + str(lrate) + " " + str(regparam) + " " + str(model.score(vfeatures, vclass)))
    print()
    testpred = model.predict(testfeatures).tolist()
    finalsave = {}
    finalsave["fname"] = "camera"
    for i, pred in enumerate(testpred):
        finalsave[imgs[i]] = pred
    with open(name + '.csv','w') as f:
        w = csv.writer(f)
        w.writerows(finalsave.items())
    print("test predictions saved")

gt = "finaltesting/nn10cor"
ps = []
l = 0.00000001
r = 0.00000001
#for l in (0.001, 0.0001):
#        r = l
#        n = 10
#        ps.append(Process(target=testModel, args = ("mlp", gt + "mlp-" + str(l) + " - " + str(r), 50000, r, l, n)))
        #ps.append(Process(target=testModel, args = ("logreg",  gt + "logreg" + str(l) + " " + str(r), 5000, r, l)))
        #ps.append(Process(target=testModel, args = ("logistic",  gt + "logistic")))
#        ps.append(Process(target=testModel, args= ("adam",  gt + "mlpa-" + str(l) + " - " + str(r), 50000, r, l, n)))



for p in ps:
    p.start()
for p in ps:
    p.join()

