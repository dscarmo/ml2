'''
Mini-library written by Diedre Carmo
Common image manipulation functions that he got tired of rewriting
'''
import time
import numpy as np
import cv2
import tkinter
from skimage import feature

resize_type = cv2.INTER_CUBIC

root = tkinter.Tk()
root.withdraw()
screen_width, screen_height = root.winfo_screenwidth(), root.winfo_screenheight()

#Receive a GrayScale image and return the LBP(Local Binary Patter) image
def generateLBP(grayImage):
    dst = np.zeros(((grayImage.shape[0]-2),(grayImage.shape[1]-2)),np.uint8)
    for i in range(1,(grayImage.shape[0]-1)):
        for j in range(1,(grayImage.shape[1]-1)):
            code = 0
            code |= (grayImage[(i-1)][(j-1)]>grayImage[i][j])<<7
            code |= (grayImage[(i-1)][(j)]>grayImage[i][j])<<6
            code |= (grayImage[(i-1)][(j+1)]>grayImage[i][j])<<5
            code |= (grayImage[(i)][(j+1)]>grayImage[i][j])<<4
            code |= (grayImage[(i+1)][(j+1)]>grayImage[i][j])<<3
            code |= (grayImage[(i+1)][(j)]>grayImage[i][j])<<2
            code |= (grayImage[(i+1)][(j-1)]>grayImage[i][j])<<1
            code |= (grayImage[(i)][(j-1)]>grayImage[i][j])<<0
            dst[(i-1)][(j-1)] = code
    return dst

def getHistogram(grayImage):
    hist = np.zeros((256,1))
    for i in range(0,grayImage.shape[0]):
        for j in range(0,grayImage.shape[1]):
            pixel = int(grayImage[i][j])
            hist[pixel] = hist[pixel]+1
    for w in range(0,256):
        hist[w] = float(hist[w]/grayImage.size)
        if(not np.isfinite(hist[w])):
            hist[w] = 0
    return hist
class LocalBinaryPatterns:
	def __init__(self, numPoints, radius):
		# store the number of points and radius
		self.numPoints = numPoints
		self.radius = radius
 
	def describe(self, image, eps=1e-7):
		# compute the Local Binary Pattern representation
		# of the image, and then use the LBP representation
		# to build the histogram of patterns
		lbp = feature.local_binary_pattern(image, self.numPoints,
			self.radius, method='uniform')
		#cv2.imshow("lbp", lbp)
		#waitForEscToQuit()
		(hist, _) = np.histogram(lbp.ravel(),
			bins=np.arange(0, self.numPoints + 3),
			range=(0, self.numPoints + 2))
 
		# normalize the histogram
		hist = hist.astype("float")
		hist /= (hist.sum() + eps)
 
		# return the histogram of Local Binary Patterns
		return hist

def correlation_coefficient(T1, T2):
	'''
	A correlation coefficient between two matrices, i think
	'''
	numerator = np.mean((T1 - T1.mean()) * (T2 - T2.mean()))
	denominator = T1.std() * T2.std()
	if denominator == 0:
		return 0
	else:
		result = numerator / denominator
		return result

def cropCenter(img, size):
	'''
	Extracts rectangle of
	size size in center of rgb img 
	'''
	cropx = size[0]
	cropy = size[1]
	y = img.shape[0]
	x = img.shape[1]
	startx = x//2 - (cropx//2)
	starty = y//2 - (cropy//2)    
	return img[starty:starty + cropy, startx:startx + cropx, :]

def uPrint(msg, verbose):
	'''
	Receives a string and only prints it if verbose is True
	'''
	if verbose is True:
		print(msg)

def uResize(img, size=None):
	'''
	Resizes the input img
	when size is not None
	'''
	uAssert(img)
	if size is None:
		return img
	else:
		return cv2.resize(img, size, resize_type)

def uSingleElementList(ulist):
	'''
	Implements "support" for single element
	lists when trying to use list parameter
	'''
	if type(ulist) is not list:
		return [ulist]
	else:
		return ulist

def uAssert(imglist):
	'''
	Checks if input array of images are not none
	Should be used only on low level functions
	'''
	imglist = uSingleElementList(imglist)
	
	for img in imglist:
		if img is None:
			raise TypeError("empty image detected in uassert")


def createMosaic(m1, m2, m3, m4, size=None):
	'''
	Tries its best to create a 4 image mosaic
	with the provided images, using fusions
	'''
	upper = imageFusion(m1, m2)
	lower = imageFusion(m3, m4)
	mosaic = imageFusion(upper, lower, horizontal=False)
	return uResize(mosaic, size)



#TODO adapt for any number of input images and create type assertion
def imageFusion(a, b, horizontal=True, return_size=None, adjustTo=0, verbose=False):
	'''
	Horizontal or vertically concat images a and b automatically, 
	even if images have different sizes.
	Size parameter does a resize in the resulting image
	In the case of different sizes, defaults to adjusting to 
	first image size, unless told othewise with adjustTo (0-a, 1-b)
	'''
	uAssert([a, b])

	arows = a.shape[0]
	acols = a.shape[1]
	brows = a.shape[0]
	bcols = a.shape[1]
	if horizontal is True:
		if arows != brows:
			uPrint("Horizontal fusion of different image heights.", verbose)
			if adjustTo == 0:
				b = cv2.resize(b, (bcols, arows))
			elif adjustTo == 1:
				a = cv2.resize(a, (acols, brows))
			else:
				raise ValueError("adjustTo should be 0 or 1")
			concat = cv2.hconcat([a, b])
		else:
			concat = cv2.hconcat([a, b])
	elif horizontal is False:
		if acols != bcols:
			uPrint("Vertifal fusion of different image widths.", verbose)
			if adjustTo == 0:
				b = cv2.resize(b, (acols, brows))
			elif adjustTo == 1:
				a = cv2.resize(a, (bcols, arows))
			else:
				raise ValueError("adjustTo should be 0 or 1")
			concat = cv2.vconcat([a, b])
		else:
			concat = cv2.vconcat([a, b])
	else:
		raise TypeError("Horizontal should be passed as True or False")
	
	return uResize(concat, return_size) 

def superPixel(img, asstr=False):
	'''
	Returns super pixel of a image,
	calculated by average of channels
	'''
	if asstr is True:
		result = str((np.average(img[:,:,0]), np.average(img[:,:,1]), np.average(img[:,:,2])))
	else:
		result = (np.average(img[:,:,0]), np.average(img[:,:,1]), np.average(img[:,:,2])) 
	return result
	

def checkColorScheme():
	'''
	Returns a super pixel representation
	of a red, green, and blue image
	to check what is the current encoding
	'''
	red = cv2.imread("red.jpg")
	green = cv2.imread("green.jpg")
	blue = cv2.imread("blue.png")
	
	print("Red average super pixel: " + superPixel(red, True))
	print("Green average super pixel: " + superPixel(green, True))
	print("Blue average super pixel: " + superPixel(blue, True))

def adaptToMonitor(img, factor=2):
	'''
	Resizes a image to fit the screen
	with a reducing factor default to
	one quarter of the screen (2)
	'''
	return uResize(img, (int(screen_width/factor), int(screen_height/factor)))

def imReadToRgb(path):
	'''
	Reads a color image using RGB instead of BGR
	Also checks for None image
	'''
	rgb = cv2.cvtColor(cv2.imread(path, cv2.IMREAD_COLOR), cv2.COLOR_BGR2RGB)
	uAssert(rgb)
	return rgb

def displayImg(img, title="untitled", fit_to_screen=True, fit_factor=2,  wait=None, size=None, destroy_after=None, bgrimage=False, verbose=True):
	'''
	Displays images using opencv as easily as possible.
	Defaults every parameter unless told otherwise.
	Destroy after if passed should be in milliseconds.
	Time for destruction starts counting after wait time.
	0 will destroy immediately after wait time.
	'''
	if verbose is not True:
		return

	if fit_to_screen is True:
		display = adaptToMonitor(img, fit_factor)
		title += " autofit"
	else:
		display = uResize(img, size)

	if bgrimage is False:
		display = cv2.cvtColor(display, cv2.COLOR_RGB2BGR)

	cv2.imshow(title, display)

	if wait is not None:
		waitForEscToQuit(wait)
	if destroy_after is not None:
		if destroy_after != 0:
			time.sleep(destroy_after/1000.0)
		cv2.destroyWindow(title)

def waitForEscToQuit(ms = 0, key = 27):
	'''
	Calls OpenCV's waitKey function
	waiting for an esc to quit the program
	defaults to press key to continue
	'''
	if cv2.waitKey(ms) == key:
			cv2.destroyAllWindows()
			quit()
